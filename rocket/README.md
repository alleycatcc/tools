- source location: https://releases.rocket.chat/latest/download

- using Debian package mongodb-org (3.6.9, from mongo repo, not Debian) 

- /etc/mongod.conf:

# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
  # --- mmapv1 is deprecated, but rocket doesn't yet officially support wiredTiger.
  # --- be careful with upgrading mongo.
  # --- if upgrading to wiredTiger, use the export/import tool to convert the data.
  engine: mmapv1
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1


# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

#security:

#operationProfiling:

replication:
  replSetName: rs01

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
