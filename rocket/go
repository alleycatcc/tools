#!/bin/bash

set -eu
set -o pipefail

_ret0=

bindir=$(realpath "$(dirname "$0")")
rocketdir="$bindir"/Rocket.Chat

. "$bindir"/functions.bash

USAGE="Usage: $0 [-i for init] [-M to not try to start mongo]"

opt_i=
opt_M=
while getopts hMi-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        i) opt_i=yes ;;
        M) opt_M=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

init () {
    chd "$rocketdir"/programs/server
    cmd yarn
}

# --- not used.
prompt () {
    local ret=$1
    local mailpass
    info "mail password?"
    read mailpass
    read -d '' "$ret" <<< "$mailpass" || true
}

get-user-name-out () {
    stat -c '%U' "$1"
}

start-mongo () {
    local i
    # --- @todo what about single files?
    for i in /var/{lib,log}/mongodb; do
        if [ ! "$(get-user-name-out "$i")" = alleycat ]; then
            error "$i is not owned by alleycat"
        fi
    done

    # cmd sudo chown alleycat -R /var/log/mongodb/
    # cmd sudo chown alleycat -R /var/lib/mongodb

    # --- --smallfiles makes it so mongo doesn't refuse to start if there's less than 3GB in /var/.
    forkit mongod --config /etc/mongod.conf --smallfiles
    cmd sleep 15
}

start-mongo-if-necessary () {
    if cmd pgrep mongod; then return; fi
    info Starting mongo
    fun start-mongo
}

start-rocket () {
    xport ROOT_URL https://rocket.alleycat.cc:3000
    xport PORT 3000
    xport NODE_ENV production

    xport MONGO_URL mongodb://localhost:27017/rocketchat?replicaSet=rs01
    xport MONGO_OPLOG_URL mongodb://localhost:27017/local?replicaSet=rs01

    chd "$rocketdir"
    cmd node main.js
}

go () {
    local do_init=$1
    local do_skip_mongo=$2
    if [ "$do_init" = yes ]; then
        fun init
    fi
    if [ ! "$do_skip_mongo" = yes ]; then
        fun start-mongo-if-necessary
    fi
    fun start-rocket
}

fun go "$opt_i" "$opt_M"
