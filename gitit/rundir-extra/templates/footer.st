<div id="footer">
    <a target='_blank' href="https://rocket.alleycat.cc">rocket</a>
  | <a target='_blank' href="https://cloud.alleycat.cc">nextcloud</a>
  | <a target='_blank' href="https://gitlab.com/alleycatcc">gitlab [public]</a>
  | <a target='_blank' href="https://gitlab.com/alleycatcc-private">gitlab [private]</a>
</div>
