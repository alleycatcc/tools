#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

opt_h=
opt_c=
while getopts hc:-: arg; do
    case $arg in
        h) opt_h=yes ;;
        c) opt_c=$OPTARG ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  opt_h=yes ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

USAGE="$0 {-c <config.py>}"

childpid=

if [ -z "$opt_c" ]; then
    error "$USAGE"
fi

run () {
    cmd "$bindir"/docker-watcher.py "$@"
}

run-help () {
    fun run -h
}

run-loop () {
    while true; do
        fun run "$@" || true
        info 'Docker watcher exited'
        cmd sleep 5
        info Restarting
    done
}

# --- this doesn't really work: it's hard to know when the signal gets
# propagated to the child and when it even takes effect at all.
# --- the best way to reboot is to kill the child process:
#     killall docker-watcher

reboot () {
    info 'Caught USR1'
    cmd sleep 5
    fun run-loop -c "$opt_c"
}

if [ "$opt_h" = yes ]; then
    fun run-help
else
    trap reboot USR1
    fun run-loop -c "$opt_c"
fi
