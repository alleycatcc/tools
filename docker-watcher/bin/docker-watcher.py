#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import proj_paths # noqa

from importlib import import_module
import os
from os import path
import sys
from sys import argv
from time import sleep

# --- pip3 install setproctitle
from setproctitle import setproctitle

from acatpy.colors import yellow, green, cyan, bright_red
from acatpy.io.io import cmd, die
from acatpy.io.speak import info, warn
from acatpy.opts import get_opts, make_opt
from acatpy.util import subn, alias_kw

from dockwatch.util_io import is_image_running, container_is_running_on_all_host_ports
from dockwatch.util_io import kill_container_if_running_on_host_port, is_network_up_on_subnet, create_network

# from config import get_config

sleep_secs = 10

def img_watcher_inner(ispec, ports_host):
    containers = is_image_running(ispec)
    iname, itag = ispec

    for container in containers:
        if container_is_running_on_all_host_ports(container, ports_host):
            info('found a container running %s:%s on %s, ok' % (green(iname), cyan(itag), ' | '.join(cyan(x) for x in ports_host)))
            return False
    if containers:
        info('found container(s) running %s:%s, but on the wrong ports; ignoring' % (yellow(iname), cyan(itag)))
    else:
        info('no containers running %s:%s, ignoring any other running containers' % (yellow(iname), cyan(itag)))

    num_killed = 0
    for port in ports_host:
        num_killed += kill_container_if_running_on_host_port(port)
    if num_killed:
        info('killed %s container(s) using the ports we need' % yellow(num_killed))
    else:
        pass
    return True

def img_watcher(img_watch):
    ispec, run_spec, start_cmd = img_watch
    port_spec = run_spec.ports()
    ports_host = [x[0] for x in port_spec]
    should_start = img_watcher_inner(ispec, ports_host)
    if not should_start: return
    info('starting container on host port(s) %s' % ' | '.join(green(x) for x in ports_host))
    cmd(start_cmd, die=True)

def net_watcher(net_watch):
    nname, nsubnet = net_watch
    if is_network_up_on_subnet(nname, nsubnet):
        info('network ok')
        return
    info('creating/repairing network')
    return create_network(nname, nsubnet)

def start_watchers(img_watches, net_watches):
    while True:
        for net_watch in net_watches:
            nname, nsubnet = net_watch
            info('checking network %s (subnet %s)' % (yellow(nname), cyan(nsubnet)))
            net_watcher(net_watch)
        for img_watch in img_watches:
            (iname, itag), *_ = img_watch
            info('checking image %s:%s' % (yellow(iname), cyan(itag)))
            img_watcher(img_watch)
        info('sleep %s' % sleep_secs)
        sleep(sleep_secs)

def start(docker_spec):
    networks, images = docker_spec.networks, docker_spec.images
    net_watches = [(
        net_spec.name,
        net_spec.subnet
    ) for net_spec in networks]
    img_watches = [(
        img_spec.image,
        img_spec.run,
        img_spec.cmd.get_start_cmd(img_spec.image, img_spec.run),
    ) for img_spec in images]

    start_watchers(img_watches, net_watches)

@alias_kw(die_arg='die')
def check_exists_and_readable(the_path, die_arg=False, extra_flags=0, pref=None):
    ok = os.access(the_path, os.R_OK | extra_flags)
    if not ok:
        pref = pref + ' ' if pref else ''
        err = "%s%s doesn't exist or is not readable." % (pref, bright_red(the_path))
        if die_arg: die(err)
        else: return warn(err)
    return ok

def go():
    setproctitle('docker-watcher')
    usage = 'Usage: %s {-c config-file}' % argv[0]
    opts = [
        make_opt('-c', '--config', dest='config', help='path to config file'),
    ]
    options, args, parser = get_opts(usage, opts)
    if args: parser.error()
    if not options.config: parser.error()
    config_path = options.config
    cnt, config_path_stub = subn(r'\.py$', '', config_path)
    if cnt != 1:
        die('Config file must end in .py')
    check_exists_and_readable(config_path, die=True)
    parent = path.dirname(config_path_stub)
    stub = path.basename(config_path_stub)
    sys.path.insert(0, parent)
    config = import_module(stub)
    start(config.get_config())


go()
