# -*- coding: utf-8 -*-

import re

from acatpy.io.io import warn

get_ports_host = lambda ports: [h for (c, h) in ports]

def regex_any_ip():
    x = '\\d{1,3}'
    y = '\\.'.join([x] * 4)
    return y

# ---
# 3000/tcp -> 0.0.0.0:4000
def parse_port_line(line, ip=regex_any_ip()):
    m = re.match('^\\s*(\\d+)\\/[a-z]+\\s+->\\s+' + ip + ':(\\d+)\\s*$', line)
    return [int(_) for _ in m.groups()] if m else None

def parse_image(text):
    m = re.match('sha256:(.+)$', text)
    if m: return m.groups()[0]
    warn("Can't parse image: %s", text)

"""
def old_is_image_running_on_any_of(ports_host, image_name):
    ports_host_wanted = [str(x) for x in ports_host]
    containers = find_container_with_ancestor(image_name)
    if not containers: return None

    def running(cont, ports_running_container):
        ports_running_container_host = get_ports_host(ports_running_container)
        return pydash.find(
            ports_running_container_host,
            lambda x: x in ports_host_wanted,
        )

    def find_cont_for_port(running_port):
        (cont, ports_running_container) = running_port
        if running(cont, ports_running_container):
            return cont

    running_ports = get_container_port_info(containers)
    return find_pred(running_ports, find_cont_for_port)
"""
