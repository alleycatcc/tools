# -*- coding: utf-8 -*-

import json
from operator import itemgetter, eq, contains

import pydash

from acatpy.colors import yellow, bright_red
from acatpy.io.io import cmd, die
from acatpy.io.speak import info
from acatpy.util import ident, intersect_with, uniq_unordered

from dockwatch.util import parse_port_line, parse_image

status_not_running = ['created', 'restarting', 'removing', 'paused', 'exited']

def docker_cmd(*spec, **kw):
    mykw = {
        'die': True,
        'capture_stdout': True,
    }
    return cmd(['docker', *spec], **{**mykw, **kw})

def find_all_tags_for_image(iname):
    ret = docker_cmd('images', '--format={{.Tag}}', iname)
    return ret.ret.stdout.splitlines()

def find_containers_for_ancestor_all_tags(iname, **kw):
    tags = find_all_tags_for_image(iname)
    # if kw.get('running') is True:
    # num_constraint = (lambda x: x < 2, 'fewer than 2')
    # else:
    # num_constraint = None
    zoek = lambda x: find_containers(
        ancestor=x,
        # num_constraint=num_constraint,
        **kw,
    )
    css = [cs for tag in tags for cs in zoek((iname, tag))]
    return uniq_unordered(css)

def find_containers(ancestor=None, running=None, num_constraint=None):
    filt = []
    if ancestor:
        aname, atag = ancestor
        filt.append('ancestor=%s:%s' % (aname, atag))
    if running is True:
        filt.append('status=running')
    elif running is False:
        filt.extend('status=%s' % x for x in status_not_running)
    filtgen = ('--filter=%s' % x for x in filt)

    ret = docker_cmd('ps', '-aq', *filtgen)
    containers = ret.ret.stdout.splitlines()
    num_found = len(containers)
    # stat = ': %s' % '|'.join(green(x) for x in containers) if num_found else '.'
    # info('Found %s container(s)%s' % (num_found, stat))
    if num_constraint is not None:
        (num_cond, num_str) = num_constraint
        if not num_cond(num_found):
            reason = ' -- wanted %s' % num_str if num_str else ''
            die('Found wrong number of containers (%s)%s' % (num_found, reason))
    return uniq_unordered(containers)

def get_ports_for_container(cid):
    ret = docker_cmd('port', cid)
    ports = ret.ret.stdout.splitlines()
    y = parse_port_line
    return [z for z in (y(x) for x in ports) if z]

def get_container_port_info(containers):
    return [(c, get_ports_for_container(c)) for c in containers]

def get_ports_host(ports):
    return [h for (c, h) in ports]

def is_image_running_on_any_of(ports_host, iname):
    ports_host_wanted = [str(x) for x in ports_host]
    containers = find_containers(ancestor=iname, running=True)
    if not containers: return None
    running_instances = get_container_port_info(containers)

    f = lambda instance: [x[1] for x in instance[1]]
    g = ident
    # --- i.e. operator.contains
    cmp = lambda running, wanted: wanted in running
    found = intersect_with(running_instances, ports_host_wanted, f, g, cmp)

    if not found: return None
    if len(found) > 1:
        die("Unexpected, too many containers match port spec %s" % ports_host)
    return found[0][0][0]

def container_inspect(cid):
    ret = docker_cmd('container', 'inspect', cid)
    return json.loads(ret.ret.stdout)

def image_inspect(iid):
    ret = docker_cmd('inspect', iid)
    return json.loads(ret.ret.stdout)

def get_ancestor(cid):
    cinfo = container_inspect(cid)
    imgid = parse_image(cinfo[0]['Image'])
    if not imgid: die()
    return imgid

def is_image_running(ispec):
    cnt = find_containers(
        ancestor=ispec,
        running=True,
    )
    return cnt

def is_latest_running(iname):
    return is_image_running((iname, 'latest'))

def container_is_running_on_any_host_port(cnt, ports_host):
    p = get_ports_for_container(cnt)
    q = intersect_with(ports_host, p, ident, itemgetter(1), eq)
    return q != []

def container_is_running_on_all_host_ports(cnt, ports_host):
    p = get_ports_for_container(cnt)
    q = intersect_with(ports_host, p, ident, itemgetter(1), eq)
    return len(q) == len(ports_host)

def killall(iname, running=None):
    cs = find_containers_for_ancestor_all_tags(iname, running=running)
    if not cs:
        return info('nothing to kill for image %s' % yellow(iname))
    info('killing containers %s' % ' | '.join(bright_red(_) for _ in cs))
    kill(*cs)

def kill(*cs):
    docker_cmd('rm', '--force', *cs, capture_stdout=False)

def find_containers_running_on_host_port(port):
    cs = find_containers(running=True)
    ll = get_container_port_info(cs)
    get_ports = lambda x: [y[1] for y in x[1]]
    to_kill = intersect_with(ll, [port], get_ports, ident, contains)
    return [x[0][0] for x in to_kill]

def kill_container_if_running_on_host_port(port):
    cs = find_containers_running_on_host_port(port)
    if not cs:
        return False
    info('killing container(s) %s' % ' | '.join(bright_red(x) for x in cs))
    kill(*cs)
    return True

# --- dies on bad json
def _network_subnet_match(nsubnet, config_json):
    o = json.loads(config_json)
    p = lambda net: net['Subnet'] == nsubnet
    return pydash.find(o, p)

def is_network_up_on_subnet(nname, nsubnet):
    nets = docker_cmd('network', 'ls', '-q').ret.stdout.splitlines()
    if not nets: return False
    for net in nets:
        dcmd = docker_cmd('network', 'inspect', net, '--format', '{{.Name}}|{{json .IPAM.Config}}')
        name, config_json = dcmd.ret.stdout.split('|', maxsplit=1)
        if name == nname and _network_subnet_match(nsubnet, config_json):
            return True
    return False

def disconnect_network(nname, cname):
    docker_cmd('network', 'disconnect', nname, cname)

# --- dies on bad json
def disconnect_network_from_all(nname, must_exist=False):
    # --- what if no networks? always one null network? xxx
    nets = docker_cmd('network', 'ls', '--format={{.Name}}').ret.stdout.splitlines()
    if not nname in nets:
        if must_exist:
            die("network doesn't exist: %s" % red(nname))
        return
    cs_json = docker_cmd('network', 'inspect', nname, '--format={{json .Containers}}').ret.stdout
    cs = json.loads(cs_json)
    for c in cs:
        disconnect_network(nname, c)

def remove_network(nname):
    docker_cmd('network', 'rm', nname)

def create_network(nname, nsubnet, remove=True):
    if remove:
        disconnect_network_from_all(nname)
        remove_network(nname)
    docker_cmd('network', 'create', nname, '--subnet', nsubnet)
