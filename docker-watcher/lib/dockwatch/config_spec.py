# -*- coding: utf-8 -*-

class DockerCmdSpec(object):
    def __init__(self, opts=None, cmd_override=None, daemonise=True):
        opts = opts or []
        cmd_override = cmd_override or []
        self._opts = opts
        self._cmd_override = cmd_override
        self._daemonise = daemonise

    def get_start_cmd(self, ispec, run_spec):
        return assemble_start_cmd_docker(ispec, run_spec, self._opts, self._cmd_override, self._daemonise)

class ShellCmdSpec(object):
    def __init__(self, cmd):
        self._cmd = cmd

    def get_start_cmd(self, *args):
        return self._cmd

def assemble_start_cmd_docker(ispec, run_spec, cmd_opts, cmd_override, daemonise):
    port_spec = run_spec.ports()
    mount_spec = run_spec.mounts()
    name = run_spec.name

    def get_arg(flag, spec):
        p = ['%s:%s' % (host, inner) for (host, inner) in spec]
        return [y for x in p for y in (flag, x)]

    opts = [*cmd_opts]
    if daemonise: opts.append('-d')
    ifull = '%s:%s' % ispec

    ports = get_arg('-p', port_spec)
    mounts = get_arg('-v', mount_spec)

    cmd = ['docker', 'run']
    if name is not None:
        cmd.extend(['--name=%s' % (name)])
    cmd.extend([*ports, *mounts, *opts, ifull, *cmd_override])
    return cmd

class PortSpec(object):
    def __init__(self, *ports):
        self.ports = ports

class MountSpec(object):
    def __init__(self, *mounts):
        self.mounts = mounts

class RunSpec(object):
    def __init__(self, ports=None, mounts=None, name=None):
        ports = ports or []
        mounts = mounts or []
        self._ports = PortSpec(*ports)
        self._mounts = MountSpec(*mounts)
        self.name = name

    def ports(self):
        return self._ports.ports

    def mounts(self):
        return self._mounts.mounts

class DockerImageSpec(object):
    def __init__(self, image=None, run=None, cmd=None):
        run = run or []
        cmd = cmd or DockerCmdSpec()
        self.image = image
        self.run = run
        self.cmd = cmd

class DockerNetworkSpec(object):
    def __init__(self, name=None, subnet=None):
        self.name = name
        self.subnet = subnet

class DockerSpec(object):
    def __init__(self, networks=None, images=None):
        networks = networks or []
        images = images or []
        self.networks = networks
        self.images = images
