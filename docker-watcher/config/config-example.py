# -*- coding: utf-8 -*-

import proj_paths # noqa

from dockwatch.config_spec import DockerSpec, DockerNetworkSpec, DockerImageSpec, RunSpec, DockerCmdSpec

def get_config():
    return DockerSpec(
        networks=[
            DockerNetworkSpec(
                name='mynet',
                subnet='10.19.11.0/24',
            ),
        ],
        images=[
            DockerImageSpec(
                image=('grne-search', 'acc'),
                run=RunSpec(
                    ports=[(8081, 8983)],
                    mounts=[],
                ),
                cmd=DockerCmdSpec(
                    opts=[
                        '--network',
                        'mynet',
                        '--ip',
                        '10.19.11.21',
                    ],
                ),
            ),
            DockerImageSpec(
                image=('grne-search', 'tst'),
                run=RunSpec(
                    ports=[(8082, 8983)],
                    mounts=[],
                ),
                cmd=DockerCmdSpec(
                    opts=[
                        '--network',
                        'mynet',
                        '--ip',
                        '10.19.11.31',
                    ],
                ),
            ),
            DockerImageSpec(
                image=('alleycat-jenkins-docker', 'latest'),
                run=RunSpec(
                    ports=[(8090, 50000), (8091, 8080)],
                    mounts=[
                        ('/var/run/docker.sock', '/var/run/docker.sock'),
                        ('/var/local/jenkins/jenkins_home', '/var/jenkins/jenkins-home'),
                    ],
                ),
            ),
            DockerImageSpec(
                image=('alleycat-jenkins-docker', 'interactive'),
                run=RunSpec(
                    ports=[(8092, 50000), (8093, 8080)],
                    mounts=[
                        ('/var/run/docker.sock', '/var/run/docker.sock'),
                        ('/var/local/jenkins/jenkins_home', '/var/jenkins/jenkins-home'),
                    ],
                ),
                cmd=DockerCmdSpec(
                    opts=[
                        '-it',
                    ],
                    cmd_override=[
                        '/bin/bash',
                    ],
                    daemonise=False,
                ),
            ),
        ],
    )
