# To build:

    cabal clean
    cabal update
    # --- -p (profiling) is optional, but recommended.
    # --- it may require reinstalling profiling versions of already existing
    # dependencies as well.
    cabal install --only-dependencies -p
    cabal configure -f executable
    cabal build

# To run
    cabal run docker-watcher-config -v0 <path-to-yaml-file>
