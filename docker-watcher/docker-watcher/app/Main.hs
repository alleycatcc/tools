module           Main
                 where

import           Control.Applicative
                 ( empty )

import           System.Environment
                 ( getArgs, getExecutablePath )

import           Control.Monad
                 ( when )

import           Text.Read
                 ( readMaybe )

import           Data.List as DL
                 ( intercalate )

import           Data.Text as DT
                 ( Text, pack )

import qualified Data.Text.IO as DTIO
                 ( putStr, putStrLn )

import           Lib ( showPython, parse )
import           Test ( runTests )

doTest = False

main :: IO ()
main = when doTest runTests >> go

pythonPreamble :: Text
pythonPreamble = DT.pack $ DL.intercalate "\n" [
    "# -*- coding: utf-8 -*-",
    "",
    "import proj_paths # noqa",
    "",
    "from dockwatch.config_spec import DockerSpec, DockerNetworkSpec, DockerImageSpec, RunSpec, DockerCmdSpec",
    "",
    "def get_config():",
    "    return "
    ]

go = do
    (path, level) <- processArgs
    go' path level

go' path level = either bad good =<< parse path where
    bad err = error $ "Unable to generate Python source: " ++ err
    good parsed = do
        DTIO.putStr pythonPreamble
        DTIO.putStrLn $ showPython level parsed

-- int' :: String -> Int
-- int' s = f s where
    -- f = g . read'
    -- g Nothing = error $ "Bad int: " ++ s
    -- g (Just n) = n
    -- read' = readMaybe

processArgs = f =<< getArgs where
    f [path] = pure (path, 4)
    f _ = fail'
    fail' = usage >> empty

usage = putStrLn . f =<< getExecutablePath where
    f exe = "Usage: " ++ exe ++ " yaml-path"
