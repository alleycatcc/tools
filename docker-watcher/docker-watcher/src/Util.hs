module           Util
                 ( (.:)
                 , (.:.)
                 , (.::)
                 , pam ) where

infixr 9 .:
(a .: b) x = a . b x
infixr 9 .:.
(a .:. b) x = a .: b x
infixr 9 .::
(a .:: b) x = a .:. b x

pam :: [(a -> b)] -> a -> [b]
pam fs x = map apply' fs where
    apply' f = f x

