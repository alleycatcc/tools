{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module           PyTypes
    ( PyExpression (PyExpressionValue, PyExpressionFuncCall)
    , PyValue (PyValueCollection, PyValueString, PyValueNumber, PyValueBoolean)
    , PyFuncCall (PyFuncCall)
    , PyKeywordArg (PyKeywordArg)
    , PyOrientation (Vertical, Horizontal)
    , ToPython
    , toPython
    , pyExpValue
    , mkFuncCallVertical
    , showPython
    , number
    , string
    , boolean
    , tuple
    , list
    , funcCall
    ) where

import           Data.Semigroup
                 ( (<>) )

import           Data.Text as DT
                 ( Text
                 , pack
                 , intercalate )

import           ShowTypes
                 ( ShowText, showText, indent )

import           Util
                 ( (.:), (.:.), (.::) )

class ToPython a where
    toPython :: a -> Either String PyExpression

class ValueLike a where
    pyValue :: PyValue -> a

class CollectionLike a where
    delimL :: a -> Text
    delimR :: a -> Text
    collPrefix :: a -> Maybe Text

data PyExpression = PyExpressionValue { pyExpValue :: PyValue }
                  | PyExpressionFuncCall { pyExpFuncCall :: PyFuncCall }
    deriving Show

data PyFuncCall = PyFuncCall { pyFuncCallIdent :: Text
                             , pyFuncCallOrient :: PyOrientation
                             , pyFuncCallArgs :: [PyExpression]
                             , pyFuncCallKwArgs :: [PyKeywordArg] }
    deriving Show

data PyKeywordArg = PyKeywordArg { pyKwArgId :: Text
                                 , pyKwArgVal :: PyExpression }
    deriving Show

data PyValue = PyValueCollection { pyValueCollection :: PyCollection }
             | PyValueString     { pyValueText :: Text }
             | PyValueNumber     { pyValueText :: Text }
             | PyValueBoolean    { pyValueText :: Text }
    deriving Show

data PyCollection = PyCollection { pyCollType :: PyCollType
                                 , pyCollOrient :: PyOrientation
                                 , pyCollValues :: [PyExpression] }
    deriving Show

data PyOrientation = Horizontal | Vertical
    deriving Show

data PyCollType = PyTuple | PyList
    deriving Show

instance ShowText PyExpression where
    showText n = f where
        f (PyExpressionValue v) = showText n v
        f (PyExpressionFuncCall v) = showText n v

instance ShowText PyValue where
    showText n = f where
        f (PyValueCollection c) = showText n c
        f (PyValueString t) = "'" <> showText n t <> "'"
        f (PyValueNumber t) = showText n t
        f (PyValueBoolean t) = showText n t

instance ShowText PyCollection where
    showText n = collect3 (showCollectionLike (n+4)) id pyCollOrient vs where
        vs = map (showText (n+4)) . pyCollValues

instance ShowText PyFuncCall where
    showText n = collect3 (showCollectionLike (n+4)) id pyFuncCallOrient args where
        args c = concat $ [ map (showText (n+4)) (pyFuncCallArgs c)
                          , map (showText (n+4)) (pyFuncCallKwArgs c) ]

instance ShowText PyKeywordArg where
    showText n c = kw <> "=" <> val where
        kw = pyKwArgId c
        val = showText n $ pyKwArgVal c

instance CollectionLike PyCollection where
    delimL c | isTuple c = "("
             | isList  c = "["
             | otherwise = error "delimL"
    delimR c | isTuple c = ")"
             | isList  c = "]"
             | otherwise = error "delimR"
    collPrefix           = const Nothing

instance CollectionLike PyFuncCall where
    delimL     = const "("
    delimR     = const ")"
    collPrefix = Just . pyFuncCallIdent

instance ValueLike Text where
    pyValue = pyValueText

instance ValueLike PyCollection where
    pyValue = pyValueCollection

showPython :: Int -> PyExpression -> Text
showPython = showText

showCollectionLike :: CollectionLike c => Int -> c -> PyOrientation -> [Text] -> Text
showCollectionLike n c orient vs = pref <> dl <> z <> dr where
    dl = delimL c
    dr = delimR c
    z = comma orient n vs
    pref = pref' $ collPrefix c
    pref' Nothing = ""
    pref' (Just s) = s

mkListVertical    :: [PyExpression] -> PyCollection
mkListVertical    = PyCollection PyList Vertical

mkListHorizontal  :: [PyExpression] -> PyCollection
mkListHorizontal  = PyCollection PyList Horizontal

mkTupleVertical   :: [PyExpression] -> PyCollection
mkTupleVertical   = PyCollection PyTuple Vertical

mkTupleHorizontal :: [PyExpression] -> PyCollection
mkTupleHorizontal = PyCollection PyTuple Horizontal

mkFuncCallVertical   ident = PyFuncCall ident Vertical
mkFuncCallHorizontal ident = PyFuncCall ident Horizontal

comma :: PyOrientation -> Int -> [Text] -> Text
comma = g where
    g Horizontal _ = DT.intercalate ", "
    g Vertical n = f where
        f [] = ""
        f xs = firstRow <> rows <> lastRow where
            firstRow = "\n"
            lastRow = "\n" <> indent (n-4) ""
            rows = intercalate "\n" $ map row xs
            row = indent (n) . (<> ",")

isList  :: PyCollection -> Bool
isList = f . pyCollType where
    f PyList = True
    f _      = False

isTuple :: PyCollection -> Bool
isTuple = f . pyCollType where
    f PyTuple = True
    f _       = False

isCollType t = f . pyCollType where f t = True

collect3 f a b c o = f (a o) (b o) (c o)

number :: (Show a, Num a) => a -> PyExpression
number = PyExpressionValue . PyValueNumber . pack . show
string = PyExpressionValue . PyValueString
boolean = PyExpressionValue . PyValueBoolean . f where
    f True = "True"
    f False = "False"
funcCall = PyExpressionFuncCall .:: PyFuncCall

collection = PyExpressionValue . PyValueCollection
tuple = collection .: PyCollection PyTuple
list = collection .: PyCollection PyList
