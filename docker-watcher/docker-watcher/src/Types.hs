{-# LANGUAGE OverloadedStrings #-}
-- allow "instance X [Y]"
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}

module           Types
                 ( ShowText
                 , showText
                 ) where

import           Data.Semigroup
                 ( (<>) )

import           Text.Printf
                 ( printf )

import           Data.String
                 ( fromString )

import           Data.Maybe
                 ( isJust, fromJust )

import           Data.Yaml as Y ( (.:)
                                , FromJSON
                                , parseJSON
                                )

import qualified Data.Yaml as Y ( Value (Object) )

import           Data.Text as DT
                 ( Text
                 , replicate
                 , intercalate )

-- import           PyTypes ( ToPython )
import           ShowTypes
                 ( ShowText, showText )

-- instance ShowText ConfigDockerSpec where
    -- showText n c = indent n g where
        -- g = "DockerSpec(" <> commaLineMaybe f <> indent n ")"
        -- f = [ showTextMaybe (n + 4) . configNetworks $ c
            -- , Just . showText (n + 4) . configImages $ c ]

-- instance ShowTextMaybe Text where
    -- showTextMaybe _ "" = Nothing
    -- showTextMaybe n txt = Just $ indent n txt

commaLine :: [Text] -> Text
commaLine = surroundIfNotEmpty "\n" . g where
    g = intercalate ",\n" . h
    h = (<> [""])

commaLineMaybe :: [Maybe Text] -> Text
commaLineMaybe = commaLine . onlyJust

-- 'preface if not empty'
surroundIfNotEmpty surr = f where
    f "" = ""
    f txt = surr <> txt

onlyJust = map fromJust . filter isJust
class ShowTextMaybe a where
    showTextMaybe :: Int -> a -> Maybe Text

