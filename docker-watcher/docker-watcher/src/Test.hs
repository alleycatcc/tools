{-# LANGUAGE OverloadedStrings #-}

module           Test
    ( runTests ) where

import           Data.Foldable
                 ( traverse_ )

import           Data.Text as DT
                 ( pack )

import qualified Data.Text.IO as DTIO
                 ( putStrLn )

import           PyTypes
                 ( PyExpression (PyExpressionValue, PyExpressionFuncCall)
                 , PyValue (PyValueCollection, PyValueString, PyValueNumber)
                 , PyKeywordArg (PyKeywordArg)
                 , PyFuncCall (PyFuncCall)
                 , PyOrientation (Vertical, Horizontal)
                 , showPython
                 , number
                 , string
                 , list
                 , tuple
                 , funcCall
                 )

argsNum = [ number 10
          , number 12 ]

argsFunc = [ funcNetworkVertical ]
argsFunc2 = [ funcNetwork2Vertical ] ++ argsList

argsList = [list Vertical listNums, list Horizontal listNums]

kwSubnet = PyKeywordArg "subnet" (string "10.0.0.1/24")
kwRxFlags = PyKeywordArg "flags" (string "re.X")
kw = []

funcNetworkVertical = funcCall "NetworkSpec" Vertical argsNum []
funcNetworkHorizontal = funcCall "NetworkSpec" Horizontal argsNum []
funcNetwork2Vertical = funcCall "NetworkSpec" Vertical (argsNum ++ argsFunc) []
funcNetwork3Vertical = funcCall "NetworkSpec" Vertical (argsNum ++ argsList ++ argsFunc2) []

listNums = [string "one", string "two", string "three"]

xxx = [ string "r'\\S+$'", string "", string "foo"]
yyy = [kwRxFlags]

funcRxH = funcCall "re.search" Horizontal xxx yyy
funcRxV = funcCall "re.search" Vertical   xxx yyy

listV = list Vertical listNums
listV2 = list Vertical [listV, listV]

tests = [ number 10
        , list Horizontal listNums
        , list Vertical listNums
        , listV2
        , list Vertical [listV2, listV2]
        , funcNetworkVertical
        , funcNetworkHorizontal
        , funcNetwork2Vertical
        , funcNetwork3Vertical
        , funcRxH
        , funcRxV
        , list Vertical [funcNetworkVertical]
        ]

runTests :: IO ()
runTests = traverse_ doTest tests

doTest exp = do
    putStrLn "---"
    -- print exp
    DTIO.putStrLn  $ showPython 0 exp
