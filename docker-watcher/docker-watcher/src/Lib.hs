module           Lib
                 ( showPython, parse ) where

import           Data.String
                 ( fromString )

import           Data.Bifunctor
                 ( first )

import qualified Data.ByteString as DBS
                 ( readFile )

import           Data.ByteString as DBS
                 ( ByteString )

import           Data.Text
                 ( Text )

import qualified Data.Yaml as Y ( ParseException, decodeEither' )

import           PyTypes
                 ( PyExpression
                 , showPython
                 , toPython
                 )

import           ConfigTypes
                 ( Config )

parseConfig :: ByteString -> Either String Config
parseConfig = either bad' good' . Y.decodeEither' where
    bad' err = Left $ "Unable to parse YAML: " ++ show err
    good' = pure

parsePython :: Config -> Either String PyExpression
parsePython = first decorate . toPython where
    decorate = ("Unable to generate Python tree: " ++)

parse :: FilePath -> IO (Either String PyExpression)
parse yamlpath = parse' <$> DBS.readFile yamlpath

parse' :: ByteString -> Either String PyExpression
parse' yaml = parsePython =<< parseConfig yaml
