{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module           ConfigTypes
    ( Config ) where

import           Data.String
                 ( fromString )

import           Data.Text as DT
                 ( Text )

import           Data.Yaml as Y ( (.:)
                                , (.:?)
                                , FromJSON
                                , parseJSON
                                )

import qualified Data.Yaml as Y ( Value (Object) )

import           Data.Semigroup
                 ( (<>) )

import           PyTypes
                 ( PyExpression (PyExpressionValue, PyExpressionFuncCall)
                 , PyOrientation (Vertical, Horizontal)
                 , PyKeywordArg (PyKeywordArg)
                 , ToPython
                 , toPython
                 , string
                 , boolean
                 , list
                 , tuple
                 , number
                 , funcCall
                 , mkFuncCallVertical
                 , pyExpValue
                 )

import           ShowTypes
                 ( ShowText, showText, indent )

import qualified Util as U
                 ( (.:) )

import           Util as U
                 ( pam )

data Config = Config { configDockerSpec :: ConfigDockerSpec
                     }

data ConfigDockerSpec = ConfigDockerSpec { configNetworks :: [ConfigNetwork]
                                         , configImages :: [ConfigImage]
                                         }

data ConfigNetwork = ConfigNetwork { configNetworkName :: Text
                                   , configNetworkSubnet :: Text
                                   }

data ConfigImage = ConfigImage { configImageName :: Text
                               , configImageTag :: Text
                               , configImageRun :: Maybe ConfigRunSpec
                               , configImageCmd :: Maybe ConfigCmdSpec
                               }

data ConfigRunSpec = ConfigRunSpec { configRunSpecPorts :: [ConfigRunPort]
                                   , configRunSpecMounts :: [ConfigRunMount]
                                   , configRunSpecName :: Maybe Text }
data ConfigRunPort = ConfigRunPort { configRunPortHost :: Int
                                   , configRunPortInner :: Int }

data ConfigRunMount = ConfigRunMount { configRunMountHost :: Text
                                     , configRunMountInner :: Text }

data ConfigCmdSpec = ConfigCmdSpec { configCmdSpecOpts :: Maybe [Text]
                                   , configCmdSpecCmd :: Maybe [Text]
                                   , configCmdSpecDaemonise :: Maybe Bool }

instance FromJSON Config where
    parseJSON (Y.Object v) = Config
        <$> key v "docker-spec"
    parseJSON _ = error "invalid type for parseJSON Config"

instance FromJSON ConfigDockerSpec where
    parseJSON (Y.Object v) = ConfigDockerSpec
        <$> key v "networks"
        <*> key v "images"
    parseJSON _ = error "invalid type for parseJSON ConfigDockerSpec"

instance FromJSON ConfigNetwork where
    parseJSON (Y.Object v) = ConfigNetwork
        <$> key v "name"
        <*> key v "subnet"
    parseJSON _ = error "invalid type for parseJSON ConfigNetwork"

instance FromJSON ConfigImage where
    parseJSON (Y.Object v) = ConfigImage
        <$> key v "image-name"
        <*> key v "image-tag"
        <*> keyOptional v "run"
        <*> keyOptional v "cmd"
    parseJSON _ = error "invalid type for parseJSON ConfigImage"

instance FromJSON ConfigRunSpec where
    parseJSON (Y.Object v) = ConfigRunSpec
        <$> key v "ports"
        <*> key v "mounts"
        <*> keyOptional v "name"
    parseJSON x = error $ "invalid type for parseJSON ConfigRunSpec: " ++ show x

instance FromJSON ConfigRunPort where
    parseJSON (Y.Object v) = ConfigRunPort
        <$> key v "port-host"
        <*> key v "port-inner"
    parseJSON _ = error "invalid type for parseJSON ConfigRunPort"

instance FromJSON ConfigRunMount where
    parseJSON (Y.Object v) = ConfigRunMount
        <$> key v "mount-host"
        <*> key v "mount-inner"
    parseJSON _ = error "invalid type for parseJSON ConfigRunMount"

instance FromJSON ConfigCmdSpec where
    parseJSON (Y.Object v) = ConfigCmdSpec
        <$> keyOptional v "opts"
        <*> keyOptional v "cmd"
        <*> keyOptional v "daemonise"
    parseJSON x = error "invalid type for parseJSON ConfigRunSpec:"

instance ToPython Config where
    toPython (Config ds) = toPython ds

instance ToPython ConfigDockerSpec where
    toPython (ConfigDockerSpec ns is) = f <$> toPython ns <*> toPython is where
        f = funcCall "DockerSpec" Vertical [] U..: kw
        kw ns' is' = [ PyKeywordArg "networks" ns'
                     , PyKeywordArg "images" is' ]

instance ToPython [ConfigNetwork] where
    toPython = Right . f where
        f = list Vertical . map spec
        spec x   = funcCall "DockerNetworkSpec" Vertical [] [kwName x, kwSubnet x]
        kwName   = PyKeywordArg "name" . string . configNetworkName
        kwSubnet = PyKeywordArg "subnet" . string . configNetworkSubnet

instance ToPython [ConfigImage] where
    toPython [] = Left "images=[] is empty"
    toPython imgs = list Vertical <$> mapM spec imgs where
        spec img = do
            kwRun <- toPythonMb . configImageRun $ img
            kwCmd <- toPythonMb . configImageCmd $ img
            let kwRun' = PyKeywordArg "run" <$> kwRun
                kwCmd' = PyKeywordArg "cmd" <$> kwCmd
                kwImg' = PyKeywordArg "image" kwImg
                imgName' = string . configImageName
                imgTag'  = string . configImageTag
                kwImg = tuple Horizontal [ imgName' img, imgTag' img ]
                kw' = concatJust [Just kwImg', kwRun', kwCmd']
            pure $ funcCall "DockerImageSpec" Vertical [] kw'

instance ToPython Text where
    toPython = pure . string

instance ToPython ConfigRunSpec where
    toPython runSpec = do
        a <- toPython . configRunSpecPorts $ runSpec
        b <- toPython . configRunSpecMounts $ runSpec
        c <- toPythonMb . configRunSpecName $ runSpec
        let a' = PyKeywordArg "ports" a
            b' = PyKeywordArg "mounts" b
            c' = PyKeywordArg "name" <$> c
        let kw = concatJust [ Just a', Just b', c' ]
        pure $ funcCall "RunSpec" Vertical [] kw

instance ToPython [ConfigRunPort] where
    toPython = pure . f where
        f = list Horizontal . map spec
        spec = tuple Horizontal . vs
        vs = pam [ number . configRunPortHost
                 , number . configRunPortInner ]

instance ToPython [ConfigRunMount] where
    toPython = pure . f where
        f = list Vertical . map spec
        spec = tuple Horizontal . vs
        vs = pam [ string . configRunMountHost
                 , string . configRunMountInner ]

instance ToPython ConfigCmdSpec where
    toPython = pure . f where
        f = funcCall "DockerCmdSpec" Vertical [] . kw
        kw = concatJust . pam [ kwList' string "opts" configCmdSpecOpts
                              , kwList' string "cmd_override" configCmdSpecCmd
                              , kwScalar' boolean "daemonise" configCmdSpecDaemonise ]
        kwScalar' = kw'
        kwList'   = kw' . list Vertical U..: map
        kw' g tag accessor o = PyKeywordArg tag . g <$> accessor o

concatJust :: [Maybe a] -> [a]
concatJust = foldr f [] where
    f (Just a) = (a :)
    f Nothing = id

toPythonMb :: ToPython a => Maybe a -> Either String (Maybe PyExpression)
toPythonMb (Just a) = Just <$> toPython a
toPythonMb Nothing = pure Nothing

key v tag         = v .: fromString tag
keyOptional v tag = v .:? fromString tag
