{-# LANGUAGE OverloadedStrings #-}

module           ShowTypes
                 ( ShowText, showText, indent ) where

import           Data.Semigroup
                 ( (<>) )

import           Data.Text as DT
                 ( Text
                 , replicate
                 )

class ShowText a where
    showText :: Int -> a -> Text

instance ShowText Text where
    showText _ = indent 0

indentX c n s = i <> s where
    i = DT.replicate n c

indent = indentX " "
