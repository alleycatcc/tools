defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  appendM, dot, side2, dot1, dot2,
  die, appendToM, join, map, tap, prop, asteriskN,
  condS, guard, lt, gt, eq, otherwise,
  id, concat, timesV, always,
  reject, notOk, noop,
  ifOk,
} from 'stick-js'

import {
  yellow, green, brightRed, getopt, sprintf,
  info, error, log,
} from 'fish-lib'

import {
  left as Left, right as Right,
} from 'bilby'

const compactOk = reject (notOk)
const toString = dot ('toString')
const on = side2 ('on')
const resolveP = (...args) => Promise.resolve (...args)
const startP = _ => null | resolveP
const then = dot1 ('then')
const recover = dot1 ('catch')
const length = prop ('length')
const flatMap = dot1 ('flatMap')
const fold = dot2 ('fold')
const toEither = l => ifOk (
  Right,
  l | Left | always,
)

const usageF = msg => () => {
    const [_, scriptName, ...args] = process.argv
    const str = join (' ', compactOk ([scriptName, msg]))
    return sprintf ("Usage: %s", str)
}

const config = {
    usage: '[-r]',
}

const usage = usageF (config.usage)

const opt = getopt ({
    h:  'b',
    r:  'b',
})

if (opt.h) {
    info (usage ())
    process.exit (0)
}

const doShowRaw = opt.r

if (opt.argv.remain.length) usage () | error

const readStdin = _ => new Promise ((resolve, reject) => {
  const buf = []
  process.stdin
    | on ('data') (toString >> appendToM (buf))
    | on ('end')  (_ => buf | join ('') | resolve)
})

const parse = JSON.parse

const space = n => ' ' | timesV (n) | join ('')

const pad = n => str => str | length | condS ([
  n | lt    | guard (l => str | concat (space (n - l))),
  otherwise | guard (str | always),
])

const entry = ({ type = 'unknown', name = 'unknown', data, }) =>
  [type, name | green, data]
  | asteriskN ([5 | pad, 40 | pad, id])
  | join (' ')

const showRaw = doShowRaw ? log : noop
const domainRecordsProp = 'domain_records'

// --- Either String Data
const getDomainRecords = (domainRecordsProp | prop) >> toEither ("couldn't get prop: " + domainRecordsProp)

const go = _ => startP ()
  | then (readStdin)
  | then (parse)
  | then (tap (showRaw))
  | then (getDomainRecords
    >> flatMap (map (entry) >> join ('\n') >> Right)
    >> fold (
      concat (' (try -s)') >> die,
      tap (log),
    )
  )
  | recover (error)

go ()
