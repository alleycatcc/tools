### for nextcloud ONLYOFFICE

- make domain which points to same machine as nextcloud, e.g.
  nextcloud-onlyoffice.domain.xx

- use this apache config:

        <VirtualHost *:80>
            ServerName nextcloud-onlyoffice.domain.xx
            ServerAdmin admin@domain.xx
            ErrorLog ${APACHE_LOG_DIR}/nextcloud-onlyoffice-error.log
            CustomLog ${APACHE_LOG_DIR}/nextcloud-onlyoffice-access.log combined
            ProxyPreserveHost off
            ProxyPass / http://localhost:3999/ nocanon
            ProxyPassReverse / http://localhost:3999/
        </VirtualHost>

        <VirtualHost *:443>
            ServerName nextcloud-onlyoffice.domain.xx
            ServerAdmin admin@domain.xx
            ErrorLog ${APACHE_LOG_DIR}/nextcloud-onlyoffice-error.log
            CustomLog ${APACHE_LOG_DIR}/nextcloud-onlyoffice-access.log combined
            ProxyPreserveHost off
            ProxyPass / http://localhost:4000/ nocanon
            ProxyPassReverse / http://localhost:4000/
        </VirtualHost>

- renew / get certificate

        # --- e.g.
        sudo certbot -d nextcloud-onlyoffice.domain.xx

- remove any changes certbot makes to the apache config (e.g. SSLCertificateFile)

- then:

        docker exec onlyoffice mv /etc/letsencrypt /etc/letsencrypt-"$(date)"
        sudo docker cp /etc/letsencrypt onlyoffice:/etc/letsencrypt

- then docker exec to onlyoffice and manually remove unneeded directories
  from /etc/letsencrypt (/archive/live/renewal etc.)
