# import re

import pcbnew as pcbnew

def always(x):
    return lambda _: x

def board():
    return pcbnew.GetBoard()

# --- s is False means ignore whether it's selected (it doesn't mean take
# only non-selected)
def _filter_selected(x, s):
    return not s or x.IsSelected()

def warn(*s):
    def f():
        yield 'Warning:'
        for x in s: yield x
    print(' '.join(f()))

# 6 = PCB_TEXT
# 7 = FP_TEXT

# --- kicad 6:
PCB_DIM_ALIGNED = 16
PCB_DIM_ORTHOGONAL = 20
PCB_SHAPE = 5
PCB_TEXT = 9

# --- even with no predicate, we need to do one iter like this with Drawings and GraphicalItems to get the types
# we need.
def iterx(ds, s=False, p=always(True)):
    return (x for x in ds if _filter_selected(x, s) and p(x))

# --- we need to use VECTOR2I to set the text size, and it wants integers,
# seems this 'IU' scale will work.
def vector2i_from_doubles(d1, d2):
    scale = pcbnew.pcbIUScale
    return pcbnew.VECTOR2I(scale.mmToIU(d1), scale.mmToIU(d2))


# --- all text items, or limit with only
# --- only: 'drawings', 'footprints', None
def text(s=False, only=None):
    if only == 'drawings':
        return get_drawing_texts(s=s)
    elif only == 'footprints':
        return get_footprint_texts(s=s)
    elif only is None:
        # return (*iterx(board().Drawings(), s=s), *get_footprint_texts(s=s))
        return *get_drawing_texts(s=s), *get_footprint_texts(s=s)
    return warn("text(): invalid value for only")

def set_text_sizes(t, s1, s2):
    t.SetTextSize(vector2i_from_doubles(s1, s2))
    refresh()
def set_text_size(t, s):
    return set_text_sizes(t, s, s)
def big_text(t):
    set_text_size(t, 0.7)
def medium_text(t):
    set_text_size(t, 0.6)
def get_drawing_texts(s=False):
    return iterx(board().Drawings(), s=s, p=lambda x: x.Type() == PCB_TEXT)
def get_footprints(s=False):
    return (x for x in board().GetFootprints() if _filter_selected(x,s))
def get_footprint_texts(s=False):
    for fp in get_footprints(s):
        # --- it's actually not really clear what this is.
        for y in iterx(fp.GraphicalItems(), s=s, p=lambda x: x.Type() == PCB_TEXT):
            yield y
        for y in (fp.Reference(), fp.Value()):
            yield y
def refresh():
    pcbnew.Refresh()

def update_all(font=None, size=None):
    for i in text():
        if font is not None:
            i.SetFont(font)
        if size is not None:
            set_text_size(i, size)
    refresh()

#ts = set_text_size
#tss = set_text_sizes
#fpt = get_footprint_texts
#fps = get_footprints

'''
To set the font: we can't seem to call SetFont directly because it's not
clear how to construct KIFONT data using the Python API. So manually set
the font of one piece of text, for example a reference, (hint: Lora Bold),
and select the footprint, and then:

    font = list(get_footprints(True))[0].Reference().GetFont()

and then

for i in text():
    i.SetFont(font)
    set_text_size(i, 0.8)

'''
