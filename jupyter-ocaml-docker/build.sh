#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0 [-n namespace] [-t tag, defaults to 'latest']"

opt_n=
opt_t=
while getopts hn:t:-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        n) opt_n="$OPTARG" ;;
        t) opt_t="$OPTARG" ;;
        -)  OPTARG_KEY="${OPTARG%=*}"
            OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG_KEY in
                help)  warn "$USAGE"; exit 0 ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

rootdir=$bindir

namespace=
tag=latest
if [ -n "$opt_n" ]; then
    namespace="$opt_n"/
fi
if [ -n "$opt_t" ]; then
    tag="$opt_t"
fi

imagename="$namespace"jupyter-ocaml:"$tag"

go () {
    mci
    mcb docker build
    # --- needs experimental syntax
    # mcb   --secret id=password,src=password.txt
    mcb   -t "$imagename"
    mcb   .
    mcg
}

cwd "$rootdir" fun go
