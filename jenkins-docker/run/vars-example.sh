# --- only used for running the container, not building it; usually we only
# build it using these scripts and run it using docker-watcher.

ports=('18080:8080' '60000:50000')
mounts=(
    # --- for sharing the host socket, so that you can build docker images from
    # within a docker container.
    '/var/run/docker.sock:/var/run/docker.sock'

    '/var/local/jenkins-home:/var/jenkins_home'
)
