Build, and optionally run, alleycatcc/jenkins-docker, which is simply a
wrapper over jenkins/jenkins to change the user to root and install Docker
inside the Docker container. This allows us to build Docker images with
Jenkins. By mounting /var/run/docker.sock into the container, the images
will then be available on the host.
