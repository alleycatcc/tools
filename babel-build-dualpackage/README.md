A tool for packaging a library as both cjs and es.

Also includes a source rewriter which is useful for rewriting import statements, because the module
system is kind of a mess. Will hopefully disappear entirely.

How it works:

Take stick-js. We want to package it as cjs and es. We work in the src/ dir with only .js files, and
they import each other when necessary using extension "import y from ./x.js". The import linter also works.
Then this tool runs Babel twice with BABEL_ENV set to es/cjs respectively. Then it runs the source
transforms and filename modifier. For the es version, it renames all .js files to .mjs and it
transforms all the import paths from .js to .mjs. It can also run custom source rewrites (see
below). The cjs version is not modified, since the source transformer is looking for import
statements, and they've already been changed to require statements by Babel.

Now take alleycat-js. It works the same as far as its own modules go, but it also depends on a
third-party module, in this case stick-js. We need to make sure that we end up with
`require('stick-js')` in the commonJs case and `import y from 'stick-js' in
the es case, and that the linter works during development. So we use the /es suffix in the
src/ files, and provide two rules to the transformer: stick-js/es -> stick-js in require
statements, stick-js/es -> stick-js in import statements.
