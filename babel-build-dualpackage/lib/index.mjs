#!/usr/bin/env -S node -r esm
import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

import { pipe, compose, composeRight, sprintf1, sprintfN, xReplace, each } from 'stick-js/es';
import fs from 'fs';
import path from 'path';
import configure from 'alleycat-js/configure';
import { parse as parseArgs } from './args.mjs';
import config from './config.mjs';
import { cwd, eachFileRecursive, green, info, mv, rmDir, setEnv, sysSpawn } from './io.mjs';
import { transform } from './transform.mjs';
import { BuildEs, BuildCjs, getBuildParams } from './types.mjs';
var configTop = pipe(config, configure.init);

var _parseArgs = parseArgs(),
    rootDir = _parseArgs.rootDir,
    srcDir = _parseArgs.srcDir,
    includeComments = _parseArgs.includeComments;

var build = function build(rootDir, srcDir, specs) {
  return pipe(specs, each(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        buildType = _ref2[0],
        stub = _ref2[1];

    var outputDir = path.join(rootDir, stub);

    var _ref3 = pipe(buildType, getBuildParams),
        _ref4 = _slicedToArray(_ref3, 3),
        extension = _ref4[0],
        babelEnv = _ref4[1],
        parserOpts = _ref4[2];

    rmDir(outputDir);
    setEnv('BABEL_ENV', babelEnv);
    sysSpawn(pipe(rootDir, sprintf1('%s/node_modules/.bin/babel')), [].concat(_toConsumableArray(includeComments ? [] : ['--no-comments']), ['--compact', 'true', '-d', outputDir, srcDir]), {
      outPrint: true
    });

    var transformAndWrite = function transformAndWrite(filename) {
      var transformed = pipe(filename, transform(parserOpts, {
        compact: true,
        comments: includeComments
      }));
      fs.writeFileSync(filename, transformed);
      info(pipe(green(filename), sprintf1('wrote %s')));
      if (extension !== '.js') mv(filename, pipe(filename, xReplace(/ \.js $/, extension)));
    };

    info('starting transforms');
    eachFileRecursive(outputDir, transformAndWrite);
  }));
};

var go = function go() {
  cwd(rootDir);
  build(rootDir, srcDir, [[BuildEs, 'es'], [BuildCjs, 'cjs']]);
};

go();