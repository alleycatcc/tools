import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _toArray from "@babel/runtime/helpers/toArray";
import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

import { pipe, compose, composeRight, dot2, deconstruct2, condS, otherwise, guard, eq, map, prop, deconstruct, invoke, noop, ifOk, recurry, applyTo1, each, xMatch, concat, xReplace, xMatchStr, xReplaceStr, againstAll, not, die } from 'stick-js/es';
import fs from 'fs';
import generateMod from '@babel/generator';
import parseMod from '@babel/parser';
import traverseMod from '@babel/traverse'; // --- weird, these 'default' props shouldn't be necessary.

var traverse = traverseMod["default"];
var generate = generateMod["default"];
var DO_BABEL_EXTENSION_FIX = false;

var getSource = function getSource(filename) {
  return fs.readFileSync(filename).toString();
};

var eachAbort = recurry(2)(function (f) {
  return function (xs) {
    var quit = false;

    var abort = function abort() {
      return quit = true;
    };

    var _iterator = _createForOfIteratorHelper(xs),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var x = _step.value;
        if (quit) break;
        f(abort, x);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  };
}); // ------ [pred, update, doNext]

var requireTransforms = [// --- @todo this is only for alleycat-js: should be provided via a
// param or config file.
[xMatchStr('^stick-js/es'), xReplaceStr('^stick-js/es', 'stick-js'), false]]; // ------ [pred, update, doNext]

var importTransforms = [].concat(_toConsumableArray(DO_BABEL_EXTENSION_FIX ? [[xMatch(/@babel/), concat('.js'), false]] : []), [// --- we use .js in our source files (so that the linter etc. works), and transform it here
// to .mjs (see README).
[xMatch(/^ \.\.?\/ /), xReplace(/ \.js $ /, '.mjs'), false], // --- explicitly use the es version of react-redux, which is a stateful module, to avoid
// having two versions of the module loaded.
[xMatch(/react-redux/), concat('/es/index.js'), false], // --- @todo this is only for alleycat-js: should be provided via a
// param or config file.
[// againstAll ([
// xMatchStr ('^stick-js'),
// xMatchStr ('^stick-js/es') >> not,
// ]),
// xReplaceStr ('^stick-js', 'stick-js/es'),
xMatchStr('^stick-js/es'), xReplaceStr('^stick-js/es', 'stick-js'), false]]);

var walkSource = function walkSource(parseOpts, generateOpts) {
  return function (source) {
    var ast = parseMod.parse(source, parseOpts);
    traverse(ast, {
      CallExpression: function CallExpression(_ref) {
        var _ref$node = _ref.node,
            callee = _ref$node.callee,
            args = _ref$node.arguments;
        if (callee.name !== 'require') return;
        if (args.length !== 1) return console.warn('Invalid number of args for require (expected 1), skipping');

        var _args = _toArray(args),
            node = _args[0],
            _ = _args.slice(1);

        if (node.type !== 'StringLiteral') return;
        pipe(requireTransforms, eachAbort(function (abort, _ref2) {
          var _ref3 = _slicedToArray(_ref2, 3),
              pred = _ref3[0],
              update = _ref3[1],
              doNext = _ref3[2];

          if (pipe(node.value, pred)) {
            node.value = pipe(node.value, update);
            if (!doNext) abort();
          }
        }));
        if (node.value !== 'stick-js/es') return;
        node.value = 'stick-js';
      },
      ImportDeclaration: function ImportDeclaration(_ref4) {
        var node = _ref4.node;
        var moduleNameNode = node.source;
        pipe(importTransforms, eachAbort(function (abort, _ref5) {
          var _ref6 = _slicedToArray(_ref5, 3),
              pred = _ref6[0],
              update = _ref6[1],
              doNext = _ref6[2];

          var cur = moduleNameNode.value;

          if (pipe(cur, pred)) {
            moduleNameNode.value = pipe(cur, update);
            if (!doNext) abort();
          }
        }));
      }
    });

    var _generate = generate(ast, generateOpts, source),
        code = _generate.code,
        map = _generate.map;

    return code;
  };
};

export var transform = function transform() {
  var parseOpts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var generateOpts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return composeRight(getSource, walkSource(parseOpts, generateOpts));
};