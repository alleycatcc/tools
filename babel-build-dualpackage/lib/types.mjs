import { pipe, compose, composeRight } from 'stick-js/es';
import daggy from 'daggy';
import { cata } from 'alleycat-js/bilby';
var Build = daggy.taggedSum('Build', {
  BuildEs: [],
  BuildCjs: []
});
var BuildEs = Build.BuildEs,
    BuildCjs = Build.BuildCjs; // :: Build -> [String |extension|, String |babelEnv|, {} |parserOpts|]

export { BuildEs, BuildCjs };
export var getBuildParams = cata({
  BuildEs: function BuildEs() {
    return ['.mjs', 'es', {
      sourceType: 'module'
    }];
  },
  BuildCjs: function BuildCjs() {
    return ['.js', 'cjs', {
      sourceType: void 8
    }];
  }
});