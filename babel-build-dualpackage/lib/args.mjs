import path from 'path';
import { pipe, compose, composeRight } from 'stick-js/es';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
export var parse = function parse() {
  var opts = yargs(hideBin(process.argv)).usage('Usage: node $0 [options]').option('d', {
    string: true,
    describe: 'root dir',
    required: true
  }).option('s', {
    string: true,
    describe: 'src dir (absolute, or relative to `root dir`)',
    "default": 'src'
  }).option('include-comments', {
    "boolean": true,
    describe: 'include comments in transformed output',
    "default": false
  }).strict().help('h').alias('h', 'help').showHelpOnFail(false, 'Specify --help for available options.').argv;
  var rootDir = path.resolve(opts.d);
  return {
    rootDir: rootDir,
    includeComments: opts.includeComments,
    srcDir: path.resolve(path.join(rootDir, opts.s))
  };
};