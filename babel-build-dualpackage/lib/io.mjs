import { pipe, compose, composeRight, sprintfN, lets, tap, recurry, each, head, appendTo, tail, filter, eq, againstAll, whenPredicate, dot, not } from 'stick-js/es';
import fs from 'fs';
import path from 'path';
import fishLib from 'fish-lib';

var logWith = function logWith(header) {
  return function () {
    var _console;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return (_console = console).log.apply(_console, [header].concat(args));
  };
};

var log = fishLib.log,
    info = fishLib.info,
    warn = fishLib.warn,
    error = fishLib.error,
    green = fishLib.green,
    yellow = fishLib.yellow,
    magenta = fishLib.magenta,
    brightRed = fishLib.brightRed,
    cyan = fishLib.cyan,
    brightBlue = fishLib.brightBlue,
    sysSpawn = fishLib.sysSpawn;
fishLib.sysSet({
  die: true,
  verbose: true,
  sync: true
}).bulletSet({
  type: 'star'
});
export { log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue, sysSpawn };
var whenAgainstAll = recurry(4)(function (preds) {
  return pipe(againstAll(preds), whenPredicate);
});
export var setEnv = function setEnv(key, val) {
  info(sprintfN('[ %s ] %s %s', [yellow('env'), brightRed(key), val]));
  process.env[key] = val;
};
export var rmDir = function rmDir(dir) {
  sysSpawn('rm', ['-rf', dir], {
    outPrint: true
  });
};
export var cwd = function cwd(_cwd) {
  info(sprintfN('[ %s ] %s', [yellow('chdir'), _cwd]));
  process.chdir(_cwd);
};

var _eachFile = recurry(4)(function (recursive) {
  return function (preds) {
    return function (dir) {
      return function (f) {
        return pipe(fs.readdirSync(dir), each(function (base) {
          return lets(function () {
            return path.join(dir, base);
          }, function (fullpath) {
            return pipe(fullpath, fs.statSync);
          }, function (_, stats) {
            return stats.isDirectory();
          }, function (fullpath, stats, isDirectory) {
            if (recursive && isDirectory) _eachFile(recursive, preds, fullpath, f);
            pipe(stats, whenAgainstAll(preds)(function () {
              return pipe(fullpath, f);
            }));
          });
        }));
      };
    };
  };
});

export var eachFile = _eachFile(false, [composeRight(dot('isDirectory'), not)]);
export var eachFileRecursive = _eachFile(true, [composeRight(dot('isDirectory'), not)]);
export var mv = function mv(oldPath, newPath) {
  info(sprintfN('[ %s ] %s -> %s', [yellow('mv'), oldPath, newPath]));
  fs.renameSync(oldPath, newPath);
};