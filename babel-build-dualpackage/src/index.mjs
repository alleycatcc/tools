#!/usr/bin/env -S node -r esm

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  sprintf1, sprintfN,
  xReplace, each,
} from 'stick-js/es'

import fs from 'fs'
import path from 'path'

import configure from 'alleycat-js/configure'

import { parse as parseArgs, } from './args.mjs'
import config from './config.mjs'
import { cwd, eachFileRecursive, green, info, mv, rmDir, setEnv, sysSpawn, } from './io.mjs'
import { transform, } from './transform.mjs'
import { BuildEs, BuildCjs, getBuildParams, } from './types.mjs'

const configTop = config | configure.init

const { rootDir, srcDir, includeComments, } = parseArgs ()

const build = (rootDir, srcDir, specs) => specs | each (
  ([buildType, stub]) => {
    const outputDir = path.join (rootDir, stub)

    const [extension, babelEnv, parserOpts] = buildType | getBuildParams

    rmDir (outputDir)
    setEnv ('BABEL_ENV', babelEnv)
    sysSpawn (
      rootDir | sprintf1 ('%s/node_modules/.bin/babel'),
      [
        ... includeComments ? [] : ['--no-comments'],
        '--compact', 'true', '-d', outputDir, srcDir,
      ],
      { outPrint: true },
    )

    const transformAndWrite = (filename) => {
      const transformed = filename | transform (
        parserOpts,
        { compact: true, comments: includeComments, },
      )
      fs.writeFileSync (filename, transformed)
      info (green (filename) | sprintf1 ('wrote %s'))
      if (extension !== '.js') mv (
        filename,
        filename | xReplace (/ \.js $/, extension),
      )
    }
    info ('starting transforms')
    eachFileRecursive (outputDir, transformAndWrite)
  },
)

const go = () => {
  cwd (rootDir)
  build (rootDir, srcDir, [
    [BuildEs, 'es'],
    [BuildCjs, 'cjs'],
  ])
}

go ()
