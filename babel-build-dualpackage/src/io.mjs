defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  sprintfN, lets, tap,
  recurry, each,
  head, appendTo, tail,
  filter, eq,
  againstAll, whenPredicate, dot, not,
} from 'stick-js/es'

import fs from 'fs'
import path from 'path'

import fishLib from 'fish-lib'

const logWith = (header) => (...args) => console.log (... [header, ...args])

const {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sysSpawn,
} = fishLib

fishLib
  .sysSet ({ die: true, verbose: true, sync: true, })
  .bulletSet ({ type: 'star' })

export {
  log, info, warn, error, green, yellow, magenta, brightRed, cyan, brightBlue,
  sysSpawn,
}

const whenAgainstAll = recurry (4) (
  (preds) => againstAll (preds) | whenPredicate,
)

export const setEnv = (key, val) => {
  info (sprintfN (
    '[ %s ] %s %s',
    [yellow ('env'), brightRed (key), val],
  ))

  process.env[key] = val
}

export const rmDir = (dir) => {
  sysSpawn (
    'rm',
    ['-rf', dir],
    { outPrint: true },
  )
}

export const cwd = (cwd) => {
  info (sprintfN (
    '[ %s ] %s',
    [yellow ('chdir'), cwd],
  ))
  process.chdir (cwd)
}

const _eachFile = recurry (4) (
  (recursive) => (preds) => (dir) => (f) => fs.readdirSync (dir) | each (
    (base) => lets (
      () => path.join (dir, base),
      (fullpath) => fullpath | fs.statSync,
      (_, stats) => stats.isDirectory (),
      (fullpath, stats, isDirectory) => {
        if (recursive && isDirectory)
          _eachFile (recursive, preds, fullpath, f)
        stats | whenAgainstAll (preds) (
          () => fullpath | f,
        )
      },
    ),
  ),
)

export const eachFile = _eachFile (
  false, [
    dot ('isDirectory') >> not,
  ],
)

export const eachFileRecursive = _eachFile (
  true, [
    dot ('isDirectory') >> not,
  ],
)

export const mv = (oldPath, newPath) => {
  info (sprintfN (
    '[ %s ] %s -> %s',
    [yellow ('mv'), oldPath, newPath],
  ))
  fs.renameSync (oldPath, newPath)
}
