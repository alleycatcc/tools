defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import daggy from 'daggy'

import { cata, } from 'alleycat-js/bilby'

const Build = daggy.taggedSum ('Build', {
  BuildEs: [],
  BuildCjs: [],
})

export const { BuildEs, BuildCjs, } = Build

// :: Build -> [String |extension|, String |babelEnv|, {} |parserOpts|]
export const getBuildParams = cata ({
  BuildEs: () => [
    '.mjs', 'es', { sourceType: 'module' },
  ],
  BuildCjs: () => [
    '.js', 'cjs', { sourceType: void 8, },
  ],
})
