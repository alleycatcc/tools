defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  dot2, deconstruct2,
  condS, otherwise, guard,
  eq, map, prop,
  deconstruct,
  invoke, noop, ifOk, recurry, applyTo1,
  each, xMatch, concat, xReplace,
  xMatchStr, xReplaceStr,
  againstAll, not,
  die,
} from 'stick-js/es'

import fs from 'fs'

import generateMod from '@babel/generator'
import parseMod from '@babel/parser'
import traverseMod from '@babel/traverse'

// --- weird, these 'default' props shouldn't be necessary.
const { default: traverse, } = traverseMod
const { default: generate, } = generateMod

const DO_BABEL_EXTENSION_FIX = false

const getSource = (filename) => fs.readFileSync (filename).toString ()

const eachAbort = recurry (2) (
  (f) => (xs) => {
    let quit = false
    const abort = () => quit = true
    for (const x of xs) {
      if (quit) break
      f (abort, x)
    }
  }
)

// ------ [pred, update, doNext]
const requireTransforms = [
  // --- @todo this is only for alleycat-js: should be provided via a
  // param or config file.
  [
    xMatchStr ('^stick-js/es'),
    xReplaceStr ('^stick-js/es', 'stick-js'),
    false,
  ]
]

// ------ [pred, update, doNext]
const importTransforms = [

  // --- babel imports (e.g. helpers) are missing an extension, which is not allowed under
  // mjs; this seems to have been a bug in @babel/runtime and is no longer necessary.
  ... DO_BABEL_EXTENSION_FIX ? [[
    xMatch (/@babel/),
    concat ('.js'),
    false,
  ]] : [],

  // --- we use .js in our source files (so that the linter etc. works), and transform it here
  // to .mjs (see README).
  [
    xMatch (/^ \.\.?\/ /),
    xReplace (/ \.js $ /, '.mjs'),
    false,
  ],

  // --- explicitly use the es version of react-redux, which is a stateful module, to avoid
  // having two versions of the module loaded.
  [
    xMatch (/react-redux/),
    concat ('/es/index.js'),
    false,
  ],

  // --- @todo this is only for alleycat-js: should be provided via a
  // param or config file.
  [
    // againstAll ([
      // xMatchStr ('^stick-js'),
      // xMatchStr ('^stick-js/es') >> not,
    // ]),
    // xReplaceStr ('^stick-js', 'stick-js/es'),
    xMatchStr ('^stick-js/es'),
    xReplaceStr ('^stick-js/es', 'stick-js'),
    false,
  ],
]

const walkSource = (parseOpts, generateOpts) => (source) => {
  const ast = parseMod.parse (source, parseOpts)

  traverse (ast, {
    CallExpression: ({ node: { callee, arguments: args, }}) => {
      if (callee.name !== 'require') return
      if (args.length !== 1) return console.warn ('Invalid number of args for require (expected 1), skipping')
      const [node, ..._] = args
      if (node.type !== 'StringLiteral') return
      requireTransforms | eachAbort (
        (abort, [pred, update, doNext]) => {
          if (node.value | pred) {
            node.value = node.value | update
            if (!doNext) abort ()
          }
        }
      )
      if (node.value !== 'stick-js/es') return
      node.value = 'stick-js'
    },
    ImportDeclaration: ({ node, }) => {
      const { source: moduleNameNode, } = node
      importTransforms | eachAbort (
        (abort, [pred, update, doNext]) => {
          const { value: cur, } = moduleNameNode
          if (cur | pred) {
            moduleNameNode.value = cur | update
            if (!doNext) abort ()
          }
        }
      )
    }
  })

  const { code, map, } = generate (ast, generateOpts, source)
  return code
}

export const transform = (parseOpts={}, generateOpts={}) => getSource >> walkSource (parseOpts, generateOpts)
