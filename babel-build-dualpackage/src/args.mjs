defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import path from 'path'

import {
  pipe, compose, composeRight,
} from 'stick-js/es'

import yargs from 'yargs'
import { hideBin, } from 'yargs/helpers'

export const parse = () => {
  const opts = yargs (hideBin (process.argv))
    .usage ('Usage: node $0 [options]')
    .option ('d', {
      string: true,
      describe: 'root dir',
      required: true,
    })
    .option ('s', {
      string: true,
      describe: 'src dir (absolute, or relative to `root dir`)',
      default: 'src',
    })
    .option ('include-comments', {
      boolean: true,
      describe: 'include comments in transformed output',
      default: false,
    })
    .strict ()
    .help ('h')
    .alias ('h', 'help')
    .showHelpOnFail (false, 'Specify --help for available options.')
    .argv
  const rootDir = path.resolve (opts.d)
  return {
    rootDir,
    includeComments: opts.includeComments,
    srcDir: path.resolve (path.join (rootDir, opts.s))
  }
}
