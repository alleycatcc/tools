const config = (modules) => ({
  presets: [
    [
      '@babel/env',
      {
        modules,
        targets: {
        },
      },
    ],
  ],
  plugins: [
    'alleycat-stick-transforms',
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
  ],
})

module.exports = (api) => {
  api.cache.forever ()
  return {
    // --- false = output es modules.
    ... config (false),
  }
}
