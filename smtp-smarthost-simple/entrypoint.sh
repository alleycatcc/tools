set -e

cmd () { echo "٭ $@"; "$@"; }

cmd python3 /entrypoint.py
cmd update-exim4.conf -v
cmd exim -bd -q15m -v
