#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

USAGE="Usage: $0"

tag=v1.1

cmd docker build -t alleycatcc/smtp-smarthost-simple:"$tag" . "$@"
cmd docker image tag alleycatcc/smtp-smarthost-simple:{"$tag",latest}
