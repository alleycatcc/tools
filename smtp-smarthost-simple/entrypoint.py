#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ipaddress import ip_address
import os
import subprocess
import sys

envs = (
    'AC_SMARTHOST_ADDRESS_CANONICAL',
    'AC_SMARTHOST_PORT',
    'AC_SMARTHOST_USER',
    'AC_SMARTHOST_PASSWORD',
    'AC_RELAY_NETS',
)

eximconf_template = '''
dc_eximconfig_configtype='satellite'
dc_other_hostnames=''
dc_local_interfaces=''
dc_relay_domains=''
dc_minimaldns='false'
CFILEMODE='644'
dc_use_split_config='true'
dc_hide_mailname='true'
dc_mailname_in_oh='true'
dc_localdelivery='mail_spool'
dc_readhost='%s'
dc_relay_nets='%s'
# --- yep, ::
dc_smarthost='%s::%s'
'''


def error(*e):
    f = ' '.join(e)
    sys.stderr.write('Error: %s\n' % f)
    sys.exit(1)

def get_env(e):
    val = os.getenv(e)
    if val is None:
        error('Missing env var:', e)
    return val

def is_valid_ip(x):
    try:
        ip_address(x)
        return True
    except ValueError:
        return False

def resolve_as_ips(ip):
    if (is_valid_ip(ip)): return [ip]
    result = subprocess.run(['dig', '+short', ip], check=True, capture_output=True)
    ips = result.stdout.decode()[0:-1]
    if not ips:
        error('Unable to resolve', ip)
    return ips.split('\n')

def get_eximconf(readhost, relay_nets, addr, port):
    xs = (y for x in relay_nets.split(',') for y in resolve_as_ips(x))
    relay_nets_ip = ';'.join(xs)
    return eximconf_template % (readhost, relay_nets_ip, addr, port)

def get_clientconf(addr, user, passw):
    x = addr.split('.')
    if (len(x) < 3):
        error('Invalid domain:', addr)
    aliases_domain = '.'.join(x[-2:])
    aliases = '*.%s' % aliases_domain
    return ':'.join((aliases, user, passw))

def write(path, x):
    with open(path, 'w') as fh:
        fh.write(x)

def go():
    addr, port, user, passw, relay_nets = (get_env(x) for x in envs)
    readhost = os.getenv('AC_READHOST') or ''
    eximconf = get_eximconf(readhost, relay_nets, addr, port)
    clientconf = get_clientconf(addr, user, passw)
    write('/etc/exim4/update-exim4.conf.conf', eximconf)
    write('/etc/exim4/passwd.client', clientconf)


go()
