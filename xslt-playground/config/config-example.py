# -*- coding: utf-8 -*-

import os.path as path

from acatpy.util import pathparent, pathjoin

this_file = path.abspath(__file__)
rootpath = pathparent(path.dirname(this_file))

def get():
    bin_path = pathjoin(rootpath, 'bin')
    return {
        'saxon_jar_path': '***',
        't2_xslt_path': pathjoin(bin_path, 't2.xslt'),
        'xml_dir_path': pathjoin(rootpath, 'xml_files', 'lemmata')
    }
