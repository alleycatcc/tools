#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
A simple script to test xslt transforms on an xml string (which is
included in this file). Output is send to /tmp/out.xml (which is cleared
before)

Note that for the saxon xslt transform (which is needed for xlst
transformations with a version of > 2), one needs a valid saxon path,
which should be given in the config
'''

import proj_paths # noqa

from functools import partial
import re
import os
from os.path import dirname

from lxml import etree

from acatpy.io.speak import info
from acatpy.io.io import (
    cmd,
)
from acatpy.xml import (
    xslt_transform, parse_xml,
    xslt_transform_saxon_java,
)

from t2_export_xslt import xslt as t2_export_xslt

from config import get as config_get

config = config_get()


# --- here one sets the xslt transformation to use

# xslt = 't2_export_xslt'
xslt = 't2_interpunction_xslt'


# --- Some helper functions

def crawl(dir):
    ret = cmd(['find', '-L', dir, '-name', '*.xml', '-print0'], capture_stdout=True, die=True)
    fns = ret.out.split('\0')[0:-1]
    return fns


to_string = partial(etree.tostring, encoding='unicode')


def transform(input_path, xslt_path):
    with open(input_path, "r") as fh_i:
        os.chdir(dirname(input_path))
        xml_input_string = fh_i.read()
        if xslt == 't2_interpunction_xslt':
            # print('xml_input_string %s' % xml_input_string)
            xml_transformed_tree = xslt_transform_saxon_java(
                xml_input_string,
                xslt_path,
                config['saxon_jar_path'],
            )
            xml_transformed_string = etree.tostring(xml_transformed_tree, encoding='unicode', pretty_print=True)
        if xslt == 't2_export_xslt':
            # @todo do something with error
            (err, xml) = parse_xml(xml_input_string)
            if err: raise Exception(err)
            xml_transformed_tree = xslt_transform(xml, t2_export_xslt(None), throw=True)
            xml_transformed_string = etree.tostring(xml_transformed_tree, encoding='unicode', pretty_print=True)
        return xml_transformed_string


def go():
    output_filename = "/tmp/out.xml"
    info('Output directory: %s' % output_filename)
    # xml filenames
    fns = crawl(config['xml_dir_path'])
    for filename in fns:
        xml_transformed_string = transform(filename,
                                           config['t2_interpunction_xslt_path'])
        # clear output file
        open(output_filename, 'w').close()
        with open(output_filename, 'a') as fh:
            fh.write(xml_transformed_string)


go()
