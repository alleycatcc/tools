
# a test-xslt transformation (currently used in t2-export of grne)

'''
`homonym_nr` is None or a number.
- no PIs or comments
- remove attributes on <contLem>
- remove <metadata/>
- <link target="">x</link> -> x
- <geen-afk>x</geen-afk> -> x
'''
def xslt(homonym_nr):
    ident = '<xsl:apply-templates select="node()|@*"/>'
    hoofdW = ident
    if homonym_nr is not None:
        hoofdW = '%d. %s' % (homonym_nr, ident)
    return '''
<xsl:stylesheet
  version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes" />
  <xsl:template match="processing-instruction()|comment()"/>
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="metadata">
    <metadata>
      <indexI>
        <xsl:value-of select="indexI/item"/>
      </indexI>
    </metadata>
  </xsl:template>
  <xsl:template match="link">
    <r>
      <xsl:apply-templates select="node()"/>
    </r>
  </xsl:template>
  <xsl:template match="hoofdW">
    <xsl:copy>{hoofdW}</xsl:copy>
  </xsl:template>
  <xsl:template match="geen-afk">
    <xsl:apply-templates select="node()"/>
  </xsl:template>
  <xsl:template match="/contLem">
    <contLem>
      <xsl:apply-templates select="node()"/>
    </contLem>
  </xsl:template>
</xsl:stylesheet>
    '''.format(hoofdW=hoofdW)

