import sys
import os

for x in 'bin', 'config', 'lib', 'lib/alleycat-py':
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', x))
