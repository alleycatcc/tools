DOCKER_MAIN_IF=docker0
DOCKER_MAIN_NETWORK=bridge
DOCKER_NETWORKS=(tutor_default)
DOCKER_MAPPINGS=(
  # --- container, network, host port, container port
  "tutor_nginx_1 tutor_default 20000 80"
)
