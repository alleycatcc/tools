#!/usr/bin/env bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash
. "$bindir"/vars.sh

USAGE="Usage: $0"

DRYRUN=no

cmd-do () {
  local table=$1; shift
  local cmd
  if [ "${DRYRUN:-}" = yes ]; then
    cmd=cmd-print
  else
    cmd=cmd
  fi
  "$cmd" iptables -t"$table" "$@"
}

cmd-nat () { cmd-do nat "$@"; }
cmd-filter () { cmd-do filter "$@"; }

get-subnet () {
  local network=$1
  cmd-capture _ret docker network inspect "$network" -f '{{(index .IPAM.Config 0).Subnet}}'
}

get-main-subnet () {
  fun get-subnet "$DOCKER_MAIN_NETWORK"
}

get-if () {
  local network=$1
  local id
  cmd-capture id docker network inspect "$network" -f '{{.Id}}'
  _ret=br-"${id:0:12}"
}

get-container-ip () {
  local network_name=$1
  local container_name=$2
  cmd-capture _ret docker inspect "$container_name" -f '{{.NetworkSettings.Networks.'"$network_name"'.IPAddress}}'
}

# --- see: iptables -tnat -S
rules-nat () {
  local docker_main_if=$1
  local docker_extra_networks
  typeset -n docker_extra_networks=$2
  local docker_mappings
  typeset -n docker_mappings=$3
  # --- e.g. 172.17.0.0/16
  fun get-main-subnet
  local docker_main_subnet=$_ret
  cmd-nat -P PREROUTING ACCEPT
  cmd-nat -P INPUT ACCEPT
  cmd-nat -P POSTROUTING ACCEPT
  cmd-nat -P OUTPUT ACCEPT
  cmd-nat -N DOCKER
  cmd-nat -A PREROUTING -m addrtype --dst-type LOCAL -j DOCKER
  cmd-nat -A OUTPUT '!' -d 127.0.0.0/8 -m addrtype --dst-type LOCAL -j DOCKER
  cmd-nat -A DOCKER -i "$docker_main_if" -j RETURN

  cmd-nat -A POSTROUTING -s "$docker_main_subnet" ! -o "$docker_main_if" -j MASQUERADE
  local docker_network
  local docker_mapping
  for docker_network in "${docker_extra_networks[@]}"; do
    # --- e.g. 172.18.0.0/16
    fun get-subnet "$docker_network"
    local docker_network_subnet=$_ret

    # --- e.g. br-ec5534e7e623
    fun get-if "$docker_network"
    local docker_network_if=$_ret
    cmd-nat -A POSTROUTING -s "$docker_network_subnet" ! -o "$docker_network_if" -j MASQUERADE

    cmd-nat -A DOCKER -i "$docker_network_if" -j RETURN
    for docker_mapping in "${docker_mappings[@]}"; do
      local container_name
      local network_name
      local host_port
      local container_port
      read container_name network_name host_port container_port <<< "$docker_mapping"
      # --- e.g. 172.18.0.2
      fun get-container-ip "$network_name" "$container_name"
      local container_ip=$_ret
      cmd-nat -A DOCKER '!' -i "$docker_network_if" -p tcp -m tcp --dport "$container_port" -j DNAT --to-destination "$container_ip":"$container_port"
    done
  done
  for docker_mapping in "${docker_mappings[@]}"; do
    local container_name
    local network_name
    local host_port
    local container_port
    read container_name network_name host_port container_port <<< "$docker_mapping"
    # --- e.g. 172.18.0.2
    fun get-container-ip "$network_name" "$container_name"
    local container_ip=$_ret
    cmd-nat -A POSTROUTING -s "$container_ip"/32 -d "$container_ip"/32 -p tcp -m tcp --dport 80 -j MASQUERADE
  done
}

# --- see:
#   iptables -tfilter -S | grep -i docker
#   iptables -tfilter -S | grep -i br-
rules-filter () {
  local docker_main_if=$1
  local docker_extra_networks
  typeset -n docker_extra_networks=$2
  local docker_mappings
  typeset -n docker_mappings=$3
  cmd-filter -N DOCKER
  cmd-filter -N DOCKER-ISOLATION-STAGE-1
  cmd-filter -N DOCKER-ISOLATION-STAGE-2
  cmd-filter -N DOCKER-USER
  cmd-filter -A FORWARD -j DOCKER-USER
  cmd-filter -A FORWARD -j DOCKER-ISOLATION-STAGE-1
  cmd-filter -A FORWARD -o "$docker_main_if" -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  cmd-filter -A FORWARD -o "$docker_main_if" -j DOCKER
  cmd-filter -A FORWARD -i "$docker_main_if" '!' -o "$docker_main_if" -j ACCEPT
  cmd-filter -A FORWARD -i "$docker_main_if" -o "$docker_main_if" -j ACCEPT

  local docker_network
  local docker_mapping
  for docker_network in "${docker_extra_networks[@]}"; do
    # --- e.g. br-ec5534e7e623
    fun get-if "$docker_network"
    local docker_network_if=$_ret
    cmd-filter -A FORWARD -o "$docker_network_if" -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    cmd-filter -A FORWARD -i "$docker_network_if" '!' -o "$docker_network_if" -j ACCEPT
    cmd-filter -A FORWARD -i "$docker_network_if" -o "$docker_network_if" -j ACCEPT
    cmd-filter -A FORWARD -o "$docker_network_if" -j DOCKER

    for docker_mapping in "${docker_mappings[@]}"; do
      local container_name
      local network_name
      local host_port
      local container_port
      read container_name network_name host_port container_port <<< "$docker_mapping"
      # --- e.g. 172.18.0.2
      fun get-container-ip "$network_name" "$container_name"
      local container_ip=$_ret
      cmd-filter -A DOCKER -d "$container_ip"/32 '!' -i "$docker_network_if" -o "$docker_network_if" -p tcp -m tcp --dport "$container_port" -j ACCEPT
    done
  done

  cmd-filter -A DOCKER-ISOLATION-STAGE-1 -i "$docker_main_if" '!' -o "$docker_main_if" -j DOCKER-ISOLATION-STAGE-2
  cmd-filter -A DOCKER-ISOLATION-STAGE-1 -i "$docker_network_if" '!' -o "$docker_network_if" -j DOCKER-ISOLATION-STAGE-2
  cmd-filter -A DOCKER-ISOLATION-STAGE-1 -j RETURN
  cmd-filter -A DOCKER-ISOLATION-STAGE-2 -o "$docker_main_if" -j DROP
  cmd-filter -A DOCKER-ISOLATION-STAGE-2 -o "$docker_network_if" -j DROP
  cmd-filter -A DOCKER-ISOLATION-STAGE-2 -j RETURN
  cmd-filter -A DOCKER-USER -j RETURN
}

fun rules-nat "$DOCKER_MAIN_IF" DOCKER_NETWORKS DOCKER_MAPPINGS
fun rules-filter "$DOCKER_MAIN_IF" DOCKER_NETWORKS DOCKER_MAPPINGS
