CLOUD_USER=USER
DOMAIN=DOMAIN

if [ -z "${PASS-}" ]; then
  error "Need to set PASS for cloud user $CLOUD_USER"
fi

pass=$(urlescape "$PASS")

xxx () {
  local quiet=$1; shift
  local method=$1; shift
  local path=$1; shift
  if [ "$quiet" = yes ]; then quiet-cmd; fi
  mci
  mcb curl
  # if [ "$quiet" = yes ]; then
  mcb   -s
  # fi
  mcb   -X "$method"
  mcb   -H 'OCS-APIRequest: true'
  # --- ok to put password here, doesn't show up in the logs
  mcb https://"$CLOUD_USER":"$pass"@"$DOMAIN"/ocs/"$path"
  mcb   "$@"
  mcg
  if [ "$quiet" = yes ]; then noisy-cmd; fi
}

verricht-v1-cloud-verbose () {
  local method=$1; shift
  local endpoint=$1; shift
  xxx no "$method" v1.php/cloud/"$endpoint" "$@"
}

verricht-v1-cloud () {
  local method=$1; shift
  local endpoint=$1; shift
  xxx yes "$method" v1.php/cloud/"$endpoint" "$@"
}

verricht-v2-files-sharing-verbose () {
  local method=$1; shift
  local endpoint=$1; shift
  xxx no "$method" v2.php/apps/files_sharing/api/v1/"$endpoint" "$@"
}
